# Eagle Resources

Design rules, cam jobs and user scripts for getting Eagle CAD to work
with a BotFactory Squink printer.

## How To

You can always get the newest version of the resourses in the
`current` folder. Older versions will be stored in the `archive`
folder.

Simply download the script/file and use it with your Eagle program.

`botfactory.dru` - design rules to ensure that you design and work
within the printer's specification.

`botfactory-pnp.ulp` - a user script that exports a centroid and
rotation file that works with Squink's pick and place system.

`botfactory-single-layer.cam` - exports all the gerber files required
for a single layer print and glue application. You need to export the
pick and place file separately.

`botfactory-two-layer.cam` - exports all the gerber files required
for a two layer print and glue application. You need to export the
pick and place file separately.
